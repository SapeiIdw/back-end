import { Application } from 'express';
import examplesRouter from './api/controllers/examples/router';
import skema_waktuRouter from './api/controllers/skema_waktu/router';
import mata_kuliahRouter from './api/controllers/mata_kuliah/router';
import dosenRouter from './api/controllers/dosen/router';
import jurusanRouter from './api/controllers/jurusan/router';
export default function routes(app: Application): void {
  app.use('/api/v1/examples', examplesRouter);
  app.use('/api/v1/jurusan', jurusanRouter);
  app.use('/api/v1/dosen', dosenRouter);
  app.use('/api/v1/mata_kuliah', mata_kuliahRouter);
  app.use('/api/v1/skema_waktu', skema_waktuRouter);
};