import mongoose from 'mongoose';

const Schema = mongoose.Schema;

export interface IJurusanModel extends mongoose.Document {
  jurusan: string;
  prodi: string[];

};

const schema = new Schema({
  jurusan: String,
  prodi: Array
}
);

export const Jurusan = mongoose.model<IJurusanModel>("Jurusan", schema);