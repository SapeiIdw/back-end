import mongoose from 'mongoose';

const Schema = mongoose.Schema;

export interface IMatKulModel extends mongoose.Document {
  kurikulum: string;
};

const schema = new Schema({
  kurikulum: String
});

export const MatKul = mongoose.model<IMatKulModel>("MatKul", schema);