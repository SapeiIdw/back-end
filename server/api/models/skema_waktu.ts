import mongoose from 'mongoose';

const Schema = mongoose.Schema;

export interface ISkemaWaktuModel extends mongoose.Document {
  name: string;
  skema: any[];
};

const schema = new Schema({
  name: String,
  skema: { type: Array, "default": []}
});

export const SkemaWaktu = mongoose.model<ISkemaWaktuModel>("SkemaWaktu", schema);