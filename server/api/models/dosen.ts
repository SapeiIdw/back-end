import mongoose from 'mongoose';

const Schema = mongoose.Schema;

export interface IDosenModel extends mongoose.Document {
  jurusan: string;
  prodi: string;
  nama: string;
  email: string;
  password: string;
  role: string;

};

const schema = new Schema({
  jurusan: String,
  prodi: String,
  nama: String,
  email: String,
  password: String,
  role: String
}
);

export const Dosen = mongoose.model<IDosenModel>("Dosen", schema);