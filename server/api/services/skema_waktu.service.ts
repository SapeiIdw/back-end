import { Types as mongooseTypes } from 'mongoose';
import L from '../../common/logger'
import * as HttpStatus from 'http-status-codes';
import * as errors from "../../common/errors";

import { SkemaWaktu, ISkemaWaktuModel } from '../models/skema_waktu';

export class SkemaWaktusService {

  async all(): Promise<ISkemaWaktuModel[]> {
    L.info('fetch all examples');

    const docs = await SkemaWaktu
      .find()
      .lean()
      .exec() as ISkemaWaktuModel[];

    return docs;
  }

  async byId(id: string): Promise<ISkemaWaktuModel> {
    L.info(`fetch skema_waktu with id ${id}`);

    if (!mongooseTypes.ObjectId.isValid(id)) throw new errors.HttpError(HttpStatus.BAD_REQUEST);

    const doc = await SkemaWaktu
      .findOne({ _id: id })
      .lean()
      .exec() as ISkemaWaktuModel;

    if (!doc) throw new errors.HttpError(HttpStatus.NOT_FOUND);

    return doc;
  }

  async create(SkemaWaktuData: ISkemaWaktuModel): Promise<ISkemaWaktuModel> {
    L.info(`create skema_waktu with data ${SkemaWaktuData}`);

    const skema_waktu = new SkemaWaktu(SkemaWaktuData);

    const doc = await skema_waktu.save() as ISkemaWaktuModel;

    return doc;
  }

  async patch(id: string, SkemaWaktuData: ISkemaWaktuModel): Promise<ISkemaWaktuModel> {
    L.info(`update skema_waktu with id ${id} with data ${SkemaWaktuData}`);

    const doc = await SkemaWaktu
      .findOneAndUpdate({ _id: id }, { $set: SkemaWaktuData }, { new: true })
      .lean()
      .exec() as ISkemaWaktuModel;

    return doc;
  }

  async remove(id: string): Promise<void> {
    L.info(`delete skema_waktu with id ${id}`);

    return await SkemaWaktu
      .findOneAndRemove({ _id: id })
      .lean()
      .exec();
  }
}

export default new SkemaWaktusService();