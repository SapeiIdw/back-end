import { Types as mongooseTypes } from 'mongoose';
import L from '../../common/logger'
import * as HttpStatus from 'http-status-codes';
import * as errors from "../../common/errors";

import { Dosen, IDosenModel } from '../models/dosen';

export class DosensService {

  async all(): Promise<IDosenModel[]> {
    L.info('fetch all examples');

    const docs = await Dosen
      .find()
      .lean()
      .exec() as IDosenModel[];

    return docs;
  }

  async byId(id: string): Promise<IDosenModel> {
    L.info(`fetch dosen with id ${id}`);

    if (!mongooseTypes.ObjectId.isValid(id)) throw new errors.HttpError(HttpStatus.BAD_REQUEST);

    const doc = await Dosen
      .findOne({ _id: id })
      .lean()
      .exec() as IDosenModel;

    if (!doc) throw new errors.HttpError(HttpStatus.NOT_FOUND);

    return doc;
  }

  async create(dosenData: IDosenModel): Promise<IDosenModel> {
    L.info(`create dosen with data ${dosenData}`);

    const dosen = new Dosen(dosenData);

    const doc = await dosen.save() as IDosenModel;

    return doc;
  }

  async patch(id: string, dosenData: IDosenModel): Promise<IDosenModel> {
    L.info(`update dosen with id ${id} with data ${dosenData}`);

    const doc = await Dosen
      .findOneAndUpdate({ _id: id }, { $set: dosenData }, { new: true })
      .lean()
      .exec() as IDosenModel;

    return doc;
  }

  async remove(id: string): Promise<void> {
    L.info(`delete dosen with id ${id}`);

    return await Dosen
      .findOneAndRemove({ _id: id })
      .lean()
      .exec();
  }
}

export default new DosensService();