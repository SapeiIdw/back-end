import { Types as mongooseTypes } from 'mongoose';
import L from '../../common/logger'
import * as HttpStatus from 'http-status-codes';
import * as errors from "../../common/errors";

import { MatKul, IMatKulModel } from '../models/mata_kuliah';

export class MatKulService {

  async all(): Promise<IMatKulModel[]> {
    L.info('fetch all mata_kuliah');

    const docs = await MatKul
      .find()
      .lean()
      .exec() as IMatKulModel[];

    return docs;
  }

  async byId(id: string): Promise<IMatKulModel> {
    L.info(`fetch mata_kuliah with id ${id}`);

    if (!mongooseTypes.ObjectId.isValid(id)) throw new errors.HttpError(HttpStatus.BAD_REQUEST);

    const doc = await MatKul
      .findOne({ _id: id })
      .lean()
      .exec() as IMatKulModel;

    if (!doc) throw new errors.HttpError(HttpStatus.NOT_FOUND);

    return doc;
  }

  async create(exampleData: IMatKulModel): Promise<IMatKulModel> {
    L.info(`create mata_kuliah with data ${exampleData}`);

    const mata_kuliah = new MatKul(exampleData);

    const doc = await mata_kuliah.save() as IMatKulModel;

    return doc;
  }

  async patch(id: string, exampleData: IMatKulModel): Promise<IMatKulModel> {
    L.info(`update mata_kuliah with id ${id} with data ${exampleData}`);

    const doc = await MatKul
      .findOneAndUpdate({ _id: id }, { $set: exampleData }, { new: true })
      .lean()
      .exec() as IMatKulModel;

    return doc;
  }

  async remove(id: string): Promise<void> {
    L.info(`delete mata_kuliah with id ${id}`);

    return await MatKul
      .findOneAndRemove({ _id: id })
      .lean()
      .exec();
  }
}

export default new MatKulService();